/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * C++ utils.
 */

#import "CppUtils.h"

void CopyMtxData(M3DMatrix44f &dst, M3DMatrix44d &src) {
	for (short i=0; i < 16; i++) {
		dst[i] = (float)src[i];
	}
}

#if TARGET_OS_EMBEDDED
// Code adapted from "iOS Augmented Reality" (25th Sep. 2011)
// http://www.jonathansaggau.com/iOSAugmentedReality2011Boston.pdf

void neon_asm_grayscale(uint8_t * __restrict dst, uint8_t * __restrict src, int n) {
	__asm__ __volatile__
	("lsr		%2, %2, #3			\n"
	 "# Build the three constants:	\n"
	 "mov		r4, #28				\n"
	 "mov		r5, #151			\n"
	 "mov		r6, #77				\n"
	 "vdup.8	d4, r4				\n"
	 "vdup.8	d5, r5				\n"
	 "vdup.8	d6, r6				\n"
	 "1:							\n"
	 "# Load 8 pixels:				\n"
	 "vld4.8	{d0-d3}, [%1]!		\n"
	 "# Do the weight average:		\n"
	 "vmull.u8	q7,	d0,	d4			\n"
	 "vmlal.u8	q7,	d1,	d5			\n"
	 "vmlal.u8	q7,	d2,	d6			\n"
	 "# Shift and store:			\n"
	 "vshrn.u16	d7,	q7,	#8			\n"
	 "vst1.8	{d7}, [%0]!			\n"
	 "subs		%2, %2, #1			\n"
	 "bne		1b					\n"
	 :
	 : "r"(dst), "r"(src), "r"(n)
	 : "r4", "r5", "r6");
}
#endif
