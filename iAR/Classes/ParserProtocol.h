/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Protocol for parser implementations.
 */

#import <Foundation/Foundation.h>

#import "Object.h"
#import "Utils.h"

@protocol ParserProtocol

/**
 * Parse obj. file into a Object object.
 *
 * @param batch The Object object
 * @param path The path of .obj file
 */
- (void)parseIntoObject:(Object *)obj fromFile:(NSString *)path;

/**
 * Parse obj. file into a Object object with especific primitive.
 *
 * @param batch The Object object
 * @param path The path of .obj file
 * @param primitive The OpenGL ES primitive
 */
- (void)parseIntoObject:(Object *)obj fromFile:(NSString *)path withPrimitive:(GLenum)primitive;

@end
