/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * App delegation class.
 */

#import "iARAppDelegate.h"

@implementation iARAppDelegate

@synthesize window;

- (void)dealloc {
	[main release];
	[window release];

	[super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	CGRect bounds = [[UIScreen mainScreen] bounds];

	window = [[UIWindow alloc] initWithFrame:bounds];
	main = [[iARViewController alloc] initWithFrame:bounds];

	[window makeKeyAndVisible];
	[window setRootViewController:main];

	return YES;
}

@end
