/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * General project types.
 */

#import <GLTools.h>

/**
 * Represents 2-dimensional sizes.
 */
typedef struct Sz {
	int width;
	int height;
};

/**
 * Represents flags used by general routines.
 */
typedef struct Flags {
	bool detailing;
	bool logging;
};

/**
 * Represents info data used by general routines.
 */
typedef struct RuntimeInfo {
	Sz inputSz;
	Sz viewportSz;
	float markerSz;
	M3DMatrix44f projection;

	Flags flags;
};
