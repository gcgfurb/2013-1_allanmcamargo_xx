/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * App view controller class.
 */

#import "iARViewController.h"

@implementation iARViewController

@synthesize interface;

- (void)dealloc {
	[warn release];

	delete objManager;

	[graphics release];
#if TARGET_OS_EMBEDDED
	[videoInput release];
#endif

	[super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
	self = [super init];
	if (self) {
		[self.view setBounds:frame];

		info.viewportSz.width = CGRectGetWidth(frame);
		info.viewportSz.height = CGRectGetHeight(frame);

		info.markerSz = 0.103;

		objManager = ObjectManager::getInstance();

#if TARGET_OS_EMBEDDED
		videoInput = [[VideoInput alloc] initWithFrame:frame andFillInfo:info];
		[self.view addSubview:videoInput.previewer];

		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didCaptureBufferProcessingFinish:)
													 name:CaptureBufferProcessingDidFinishNotification
												   object:nil];
		[videoInput startCapturing];
#endif

		[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didDeviceOrientationChange:)
													 name:UIDeviceOrientationDidChangeNotification
												   object:nil];

		graphics = [[GLView alloc] initWithFrame:frame andFillInfo:info];
		[self.view addSubview:graphics];

		warn = [[UIImageView alloc] initWithFrame:frame];
		[warn setImage:[UIImage imageNamed:@"landscape_warn.png"]];
		[self.view addSubview:warn];

		detectionTime = 0.0;

		[self setup];
		[self customize];
	}
	return self;
}

- (void)setup {
#if TARGET_OS_EMBEDDED
	cameraParams.readFromXMLFile([Utils getStrResourcePathForFile:@"camera" ofType:@"yml"]);
	cameraParams.resize(cvSize(info.viewportSz.width, info.viewportSz.height));

	M3DMatrix44d tempMtx;
	detector.glGetProjectionMatrix(cameraParams,
								   cvSize(info.viewportSz.width, info.viewportSz.height),
								   cvSize(info.viewportSz.width, info.viewportSz.height),
								   tempMtx,
								   0.05,
								   10.0);
	CopyMtxData(info.projection, tempMtx);
#endif
}

- (void)customize {
	WavefrontParser *parser = [[WavefrontParser alloc] init];

	Object *cube = objManager->addObject(0);
	[parser parseIntoObject:cube fromFile:@"cube"];

	Object *sphere = objManager->addObject(512);
	[parser parseIntoObject:sphere fromFile:@"sphere"];

	Object *monkey = objManager->addObject(1023);
	[parser parseIntoObject:monkey fromFile:@"monkey"];

	CGRect frame = [self.view bounds];

	NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"panel"
													  owner:self
													options:nil];
	interface = [nibViews objectAtIndex:0];    
	interface.transform = CGAffineTransformMakeTranslation((CGRectGetWidth(frame) / 2) - (CGRectGetWidth(interface.bounds) / 2),
                                                           (CGRectGetHeight(frame) / 2) - (CGRectGetHeight(interface.bounds) / 2));
	interface.transform = CGAffineTransformRotate(interface.transform, 90.0 / 180.0 * M_PI);

	[self.view addSubview:interface];
	interface.hidden = YES;

	info.flags.detailing = true;
	info.flags.logging = false;
	((UIView *)[interface viewWithTag:1]).hidden = YES;

	[NSThread detachNewThreadSelector:@selector(refresh)
							 toTarget:self
						   withObject:nil];
}

- (void)refresh {
	UILabel *timeLbl = (UILabel *)[interface viewWithTag:2];
	UILabel *fpsLbl = (UILabel *)[interface viewWithTag:3];

	while (![[NSThread currentThread] isCancelled]) {
		if (!info.flags.logging) {
			continue;
		}
        
        NSString* timeStr = [[NSString alloc] initWithFormat:@"%.2f ms detecting", detectionTime];
        NSString* fpsStr = [[NSString alloc] initWithFormat:@"%.2f FPS", (1000.0 / detectionTime)];
        
		@synchronized(self) {
			[timeLbl performSelectorOnMainThread:@selector(setText:)
									  withObject:timeStr
								   waitUntilDone:NO];
			[fpsLbl performSelectorOnMainThread:@selector(setText:)
									 withObject:fpsStr
								  waitUntilDone:NO];
		}
        
        [timeStr release];
        [fpsStr release];
	}
}

- (IBAction)didDetailingButtonClick:(id)sender {
	info.flags.detailing = !info.flags.detailing;
}

- (IBAction)didLoggingButtonClick:(id)sender {
	info.flags.logging = !info.flags.logging;

	UIView *view = (UIView *)[interface viewWithTag:1];
	if (info.flags.logging) {
		view.hidden = NO;
	}
	else {
		view.hidden = YES;
	}
}

- (void)didDeviceOrientationChange:(NSNotification *)notification {
#if TARGET_OS_EMBEDDED
	switch ([[UIDevice currentDevice] orientation]) {
		case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
			[videoInput startCapturing];
			videoInput.previewer.hidden = NO;
			graphics.hidden = NO;
			warn.hidden = YES;

			interface.hidden = NO;

			break;
		default:
			[videoInput stopCapturing];
			videoInput.previewer.hidden = YES;
			graphics.hidden = YES;
			warn.hidden = NO;

			interface.hidden = YES;

			break;
	}
#endif
}

- (void)didCaptureBufferProcessingFinish:(NSNotification *)notification {
#if TARGET_OS_EMBEDDED
	@synchronized(self) {
		NSTimeInterval timer = [NSDate timeIntervalSinceReferenceDate];

		try {
			detector.detect([videoInput buffer], markers, cameraParams, info.markerSz);
		}
		catch (std::exception &e) {
			NSLog(@"Exception: %s", e.what());
		}

		detectionTime = ([NSDate timeIntervalSinceReferenceDate] - timer) * 1000.0;

		for (int i=0; i < markers.size(); i++) {
			Object *obj;
			if (!(obj = objManager->objectWithID(markers[i].id))) {
				continue;
			}
			M3DMatrix44d tempMtx;
			markers[i].glGetModelViewMatrix(tempMtx);

			obj->copyModelview(tempMtx);
		}
	}

	[graphics refreshWithMarkers:markers];
#endif
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [[[event allTouches] allObjects] objectAtIndex:0];
	firstTouch = [touch locationInView:self.view];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [[[event allTouches] allObjects] objectAtIndex:0];
	float dist = firstTouch.y - [touch locationInView:self.view].y;

	for (std::map<int, Object *>::iterator o = objManager->objects.begin(); o != objManager->objects.end(); o++) {
		Object *obj = o->second;
		if (!obj) {
			continue;
		}
		obj->rotate(dist);
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[self touchesMoved:touches withEvent:event];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

@end
