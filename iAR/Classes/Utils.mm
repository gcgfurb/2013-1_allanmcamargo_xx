/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Objective-C++ utils.
 */

#import "Utils.h"

@implementation Utils

+ (std::string)getStrResourcePathForFile:(NSString *)filename ofType:(NSString *)ext {
	NSString *resourcePath = [[NSBundle mainBundle] pathForResource:filename
															 ofType:ext];
	if (!resourcePath) {
		NSLog(@"File didn't found: %s.%s", filename, ext);
		return "";
	}
	return [resourcePath cStringUsingEncoding:NSUTF8StringEncoding];
}

// Code adapted from "How to capture video frames from the camera as images
// using AV Foundation" (20th Sep. 2011)
// http://developer.apple.com/library/ios/#qa/qa1702/_index.html

+ (cv::Mat)createMatFromSampleBuffer:(CMSampleBufferRef)sampleBuffer withROI:(cv::Rect)roi {
	CVImageBufferRef imgBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
	CVPixelBufferLockBaseAddress(imgBuffer, 0);

	uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imgBuffer);

	size_t width = CVPixelBufferGetWidth(imgBuffer);
	size_t height = CVPixelBufferGetHeight(imgBuffer);

	cv::Mat img(cvSize(width, height), CV_8UC4, baseAddress);
	cv::Mat roiImg(img, roi);

	return roiImg;
}

+ (UIImage *)createUIImageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer {
	CVImageBufferRef imgBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
	CVPixelBufferLockBaseAddress(imgBuffer, 0);

	uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imgBuffer);

	size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imgBuffer);
	size_t width = CVPixelBufferGetWidth(imgBuffer);
	size_t height = CVPixelBufferGetHeight(imgBuffer);

	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

	CGContextRef context = CGBitmapContextCreate(baseAddress,
												 width,
												 height,
												 8,
												 bytesPerRow,
												 colorSpace,
												 kCGBitmapByteOrder32Little |
												 kCGImageAlphaPremultipliedFirst);

	CGImageRef quartzImg = CGBitmapContextCreateImage(context);
	CVPixelBufferUnlockBaseAddress(imgBuffer, 0);

	CGContextRelease(context);
	CGColorSpaceRelease(colorSpace);

	UIImage *uiImg = [UIImage imageWithCGImage:quartzImg];
	CGImageRelease(quartzImg);

	return uiImg;
}

+ (UIImage *)createUIImageFromMat:(cv::Mat)img {
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	NSData *data = [NSData dataWithBytes:img.data length:img.elemSize() * img.total()];

	CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);

	CGImageRef imgRef = CGImageCreate(img.cols,
									  img.rows,
									  8,
									  8 * img.elemSize(),
									  img.step.p[0],
									  colorSpace,
									  kCGBitmapByteOrder32Little |
									  kCGImageAlphaNoneSkipFirst,
									  provider,
									  nil,
									  NO,
									  kCGRenderingIntentDefault);

	UIImage *uiImg = [UIImage imageWithCGImage:imgRef];

	CGImageRelease(imgRef);
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(colorSpace);

	return uiImg;
}

+ (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
}

+ (void)saveUIImage:(UIImage *)img {
	UIImageWriteToSavedPhotosAlbum(img,
								   self,
								   @selector(image:didFinishSavingWithError:contextInfo:),
								   nil);
}

@end
