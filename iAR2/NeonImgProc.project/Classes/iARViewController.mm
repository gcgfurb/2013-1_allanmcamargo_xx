/**
 * @file
 * @author  Jonathan Hess (2011/2) <jhess666@gmail.com>
 * @author  Allan Camargo (2013/1)
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * App view controller class.
 */

#import "iARViewController.h"
#import "aRMUtilz.h"
#import "StopWatch.hpp"

@implementation iARViewController

@synthesize previewer;
@synthesize interface;
@synthesize currFrame;

- (void)dealloc {
#if TARGET_OS_EMBEDDED
	[videoInput release];
#endif

	[super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
	self = [super init];
	if (self) {
		[self.view setBounds:frame];

        info.flags.benchMark = YES;
		info.viewportSz.width = CGRectGetWidth(frame);
		info.viewportSz.height = CGRectGetHeight(frame);

#if TARGET_OS_EMBEDDED
		videoInput = [[VideoInput alloc] initWithFrame:frame andFillInfo:info];
        previewer = [[UIImageView alloc] initWithFrame:frame];
		[self.view addSubview:previewer];

		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didCaptureBufferProcessingFinish:)
													 name:CaptureBufferProcessingDidFinishNotification
												   object:nil];
		[videoInput startCapturing];
#endif

		[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didDeviceOrientationChange:)
													 name:UIDeviceOrientationDidChangeNotification
												   object:nil];

		[self setup];
		[self customize];
	}
	return self;
}

- (void)setup {
#if TARGET_OS_EMBEDDED

#endif
}

- (void)customize {
	[NSThread detachNewThreadSelector:@selector(refresh)
							 toTarget:self
						   withObject:nil];
}

- (void)refresh {
}

- (void)didDeviceOrientationChange:(NSNotification *)notification {
#if TARGET_OS_EMBEDDED
    info.flags.benchMark = !info.flags.benchMark;
#endif
}

- (void)didCaptureBufferProcessingFinish:(NSNotification *)notification {
#if TARGET_OS_EMBEDDED
	@synchronized(self) {

		try {
            cv::Mat input = [videoInput buffer];
            cv::Size sz = input.size();
            cv::Mat grey(sz, CV_8UC1);
            
            int numPixels = input.cols * input.rows;
            
            uchar *outData = grey.data;
            uchar *inData = input.data;
            
            neon_asm_grayscale(outData, inData, numPixels);
            
            grey.data = outData;
            
            cv::Mat output(sz, CV_8UC1);
            
            StopWatch s;
            
            s.start();
			
//            if(info.flags.benchMark)
//                asmNeonAdaptiveThreshold1(grey, output, 255, 7, 7);
//                boxFiltering2(grey.data, output.data, sz.width, sz.height);
//                neonAdaptiveThreshold(grey, output, 255, 7, 7);
//            else
//                asmNeonAdaptiveThreshold2(grey, output, 255, 7, 7);
//                cv::boxFilter(grey, output, grey.type(), cv::Size(7, 7), cv::Point(-1,-1), true, cv::BORDER_REPLICATE);
            asmNeonAdaptiveThreshold(grey, output, 255, 7);
        
            
            
//			Static threshold, contours won't even appear on the buffer
//			thresh_8u(grey, output, 125, 255);
            
//			Adaptive, highlights contours
//			neon_asm_adaptive_threshold (grey, output, 255, 7, 7);
            s.mark("THRESHOLD");
            
            s.outputMarkers();
            
            [currFrame release];
            currFrame = [Utils createUIImageFromMat:output];
            
            [self.previewer performSelectorOnMainThread:@selector(setImage:) withObject:currFrame waitUntilDone:YES];
		}
		catch (std::exception &e) {
			NSLog(@"Exception: %s", e.what());
		}

#endif
	}
}

- (BOOL)shouldAutorotate
{
    return NO;
}

@end
