/**
 * @file    arucoARMUtilz.h
 * @author  Allan Camargo
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Header for ARM specific code, including assembler.
 */

#ifndef iAR2_arucoARMUtilz_h
#define iAR2_arucoARMUtilz_h

#include <arm_neon.h>
#include <opencv/cv.h>
#include <iostream.h>
#include "StopWatch.hpp"
#include <Accelerate/Accelerate.h>

/**
 * @author  Jonathan Hess (2011/2) <jhess666@gmail.com>
 * @author	Allan Camargo (2013/1) (minor update, improved readability by using identifiers %[src] and %[dst]
 *
 * Convert image to grayscale with NEON + ASM.
 * Code adapted from "iOS Augmented Reality" (25th Sep. 2011)
 * http://www.jonathansaggau.com/iOSAugmentedReality2011Boston.pdf
 *
 * @param dst The destination data
 * @param src The source data
 * @param n The total number of pixels
 */
void neon_asm_grayscale(uint8_t * __restrict dst, const uint8_t * __restrict src, const int n)
{
	__asm__ __volatile__
	("lsr		%2, %2, #3			\n" // divide the total of pixels by 8 (we will process 8 pixels at a time), using shift
	 "# Build the three constants:	\n"
	 "mov		r4, #28				\n"
	 "mov		r5, #151			\n"
	 "mov		r6, #77				\n"
	 "vdup.8	d4, r4				\n" // store constants in NEON registers
	 "vdup.8	d5, r5				\n"
	 "vdup.8	d6, r6				\n"
	 "1:							\n" // main loop
	 "# Load 8 pixels:				\n"
	 "vld4.8	{d0-d3}, [%[src]]!	\n" // loads 3(RGB) NEON lanes with 8 values each...
	 "# Do the weight average:		\n" // ...from the pointer %[src] and then increments (!) it by the amount used by the instruction.
	 "vmull.u8	q7,	d0,	d4			\n" // multiplies
	 "vmlal.u8	q7,	d1,	d5			\n" // multiplies then accumulates
	 "vmlal.u8	q7,	d2,	d6			\n"
	 "# Shift and store:			\n"
	 "vshrn.u16	d7,	q7,	#8			\n" // since q7 is a quad-word register, we need to shift our values back to 8 bits
	 "vst1.8	{d7}, [%[dst]]!		\n" // save the resulting 8 pixels to our destination pointer %[dst]
	 "subs		%2, %2, #1			\n" // subtract 1 from our counter, and update our condition flags (sub[s])
	 "bne		1b					\n" // jump back to main loop if the flags say the last comparison wasn't equal to zero
	 :
	 : [dst] "r" (dst), [src] "r" (src), "r"(n)
	 : "memory", "cc", "r4", "r5", "r6");
}

/**
 *  @author Allan Camargo (2013/1)
 *
 *  Filters the input image using the "mean" method.
 *	This is the result of a series of optimisations.
 *	Using NEON intrinsics it is possible to process 8 pixels at a time.
 *
 *	@param src 		Source data
 *	@param dst 		Destination pointer
 *	@param width	Width of the image
 *	@param height	Height of the image
 */
void neonMeanFilter(uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    const int indices[9] = { -(width + 1), -width, -(width - 1),
        -1,      0,    	    1,
        width - 1,  width,    width + 1};
    
    uint8_t* p = src + width; //skip the first line
    uint8_t* d = dst + width;
    const uint8_t* pEnd = src + (width * height);
    
    while(p < pEnd)
    {
        int i = 0;
        uint8x8_t greyAcc = vdup_n_u8(0);
        while (i < 9)
        {
            const uint8_t* data = p + indices[i];
         	const uint8x8_t valuesN = vld1_u8(data);
            
            //          Since we are applying a mean filter, we need to divide the values by the amount of neighbour pixels which is 9.
            //          To acheive the best results, the only operations used here were bit shifts and a subtraction.
            //          Since we can only divide by powers of 2 (using bit shifts) an approximation is needed, hence the subtraction.
            //          All of this was possible because we are handling unsigned integer values, and our ultimate goal is to have binary values for the pixels, 0 or 255 (black or white)
            //
            //          int8_t shifted = i;
            //
            //          shifted >>= 3;
            //          shifted = i - shifted;
            //          shifted >>= 3;
            
            uint8x8_t shifted = vshr_n_u8(valuesN, 3); // shift right, 3 positions (in other words, divide by 8)
			shifted = vsub_u8(valuesN, shifted);	   // remove the difference, so the next shift will be closer to our target
			greyAcc = vsra_n_u8(greyAcc, shifted, 3);  // shift right 3 again and then accumulate
            
            ++i;
        }
        
        vst1_u8(d, greyAcc);
        
        p += 8;
	    d += 8;
    }
}

void neonBoxFilter(uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    const int indices[4] = { -(width + 1), -(width - 1), width - 1, width + 1 };
    
    uint8_t* p = src + width;
    uint8_t* d = dst + width;
    const uint8_t* pEnd = src + (width * height);
    
    while(p < pEnd)
    {
        int i = 0;
        uint8x8_t greyAcc = vdup_n_u8(0);
        while (i < 4)
        {
         	uint8_t* data = p + indices[i];
            uint8x8_t valuesN = vld1_u8(data);
            vsra_n_u8(greyAcc, valuesN, 2);
            ++i;
        }
        
        vst1_u8(d, greyAcc);
        
        p += 8;
	    d += 8;
    }
}

void asmNeonMeanFilter(const uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    int indices[9] = { -(width + 1), -width, -(width - 1),
                                 -1,      0,    	    1,
                          width - 1,  width,    width + 1};
    
	__asm__ __volatile__
	("mul       r4, %[w], %[h]        \n" // r4 will be the first counter, starting at the size of the buffer
     "lsr       r4, r4, #3            \n" // dividing r4 by 8 since we'll process 8 pixels at a time
     "add       %[src], %[src], %[w]  \n" // skip the first line of pixels
     "add       %[dst], %[dst], %[w]  \n" // skip the first line of pixels
     "1:                              \n" // label 1
     "mov       r5, #9                \n" // r5 will be the second counter, starting at the size of the indices array
     "mov       r6, %[i]              \n" // r6 will point to (indices) in the loop
     "vmov.u8   d0, #0                \n" // d0 will be the accumulator
     "2:                              \n" // label 2
     "ldr       r7, [r6]              \n"
     "add       r8, %[src], r7        \n" // same as "const uint8_t* data = p + indices[i];"
     "vld1.8    {d1}, [r8]            \n" // d1 will contain the loaded data
     "vshr.u8   d2, d1, #3            \n" // shift right, 3 positions (in other words, divide by 8) 
     "vsub.i8   d2, d1, d2            \n" // remove the difference, so the next shift will be closer to our target
     "vsra.u8   d0, d2, #3            \n" // shift right 3 again and then accumulate
     "add       r6, #4                \n" // increment r6
     "subs      r5, r5, #1            \n" // decrement r5
     "bne       2b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 2
     "add       %[src], #8            \n" // increment (src) pointer
     "vst1.8    {d0}, [%[dst]]!       \n" // store the result in (dst)
     "subs      r4, r4, #1            \n" // decrement r4
     "bne       1b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     :
	 : [src] "r" (src), [dst] "r" (dst), [i] "r" (indices), [w] "r" (width), [h] "r" (height)
	 : "memory", "cc", "r4", "r5", "r6", "r7", "r8");
    /*
     
     Parameter block:
     : // output registers
     : // input registers
     : // clobber list
     
     
     - Output registers, none used
     - Input registers, here we pass on to the compiler what we will use from the memory
     - Clobber list, here we tell the compiler what the fuck we are going to do with the processor's registers
       This list is like a restaurant menu, we say:
       "memory"     : I would like to mess around with the memory
       "cc"         : I am going to take these condition flags, and do whatever the fuck I please with them
       "r4", "r5"...: I am going to have these registers for my breakfast, extra spicy
     
     */
}

void boxFiltering(const uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    float denominator = 0.0f;
    float grey;
    float greySrc;
    int iGrey;
    int indexOffset;
    
    /*
     const uint8_t kernel[9] = { -1, -1, -1,
     -1,  8, -1,
     -1, -1, -1};
     */
    
    /*
    const uint8_t kernel[9] = { 1,  1,  1,
        1,  2,  1,
        1,  1,  1};
     */
     
    // BLUR KERNEL
    const uint8_t kernel[9] = { 1,  0,  1,
                                0,  0,  0,
                                1,  0,  1};
    
 
    /*
    const uint8_t kernel[9] = { 0,  0,  0,
                                0,  1,  0,
                                0,  0,  0};
    */
    
    /* RAISE EDGES
     const uint8_t kernel[9] = { 0,  0, -2,
     0,  2,  0,
     1,  0,  0};
     */
    
    int indices[9] = { -(width + 1), -width, -(width - 1),
        -1,      0,    	    1,
        width - 1,  width,    width + 1};
    
    for (int i = 0;i < sizeof(kernel);i++)
    {
        denominator += kernel[i];
    }
    
    
    for(int i = 0; i < height - 1; i++)
    {
        for (int j = 1;j < width - 1;j++)
        {
     	    grey = .0f;
     	    indexOffset = (i * width) + j;
            
            for (int k = 0;k < sizeof(kernel);k++)
            {
                greySrc = src[indexOffset+indices[k]];
            	grey += greySrc * kernel[k];
            }
            
            iGrey = (uint8_t)(grey / denominator);
            dst[indexOffset] = iGrey;
        }
    }
}

void asmNeonBoxFilterBlurKernel(const uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    const int indices[4] = { -(width + 1), -(width - 1), width - 1, width + 1 };
    
	__asm__ __volatile__
	("mul           r4, %[w], %[h]        \n" // r4 will be the first counter, starting at the size of the buffer
     "lsr           r4, r4, #3            \n" // dividing r4 by 8 since we'll process 8 pixels at a time
     "add           %[src], %[src], %[w]  \n" // skip the first line of pixels
     "add           %[dst], %[dst], %[w]  \n" // skip the first line of pixels
     "1:                                  \n" // label 1
     "mov           r5, #4                \n" // r5 will be the second counter, starting at the size of the indices array
     "mov           r6, %[i]              \n" // r6 will point to (indices) in the loop
     "vmov.u8       d0, #0                \n" // d0 will be the accumulator
     "2:                                  \n" // label 2
     "ldr           r7, [r6]              \n"
     "add           r8, %[src], r7        \n" // same as "const uint8_t* data = p + indices[i];"
     "vld1.8        {d1}, [r8]            \n" // d1 will contain the loaded data
     "vsra.u8       d0, d1, #2            \n" // shift right 2 (divide by 4) again and then accumulate
     "add           r6, #4                \n" // increment r6
     "subs          r5, r5, #1            \n" // decrement r5
     "bne           2b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 2
     "vst1.8        {d0}, [%[dst]]!       \n" // store the result in (dst)
     "add           %[src], #8            \n" // increment (src) pointer
     "subs          r4, r4, #1            \n" // decrement r4
     "bne           1b                    \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     :
	 : [src] "r" (src), [dst] "r" (dst), [i] "r" (indices), [w] "r" (width), [h] "r" (height)
	 : "memory", "cc", "r4", "r5", "r6", "r7", "r8");
}

void boxFiltering2(const uint8_t* __restrict src, uint8_t* __restrict dst, const int width, const int height)
{
    int grey;
    int indexOffset;
    
    const int indices[4] = { -(width + 1), -(width - 1), width - 1, width + 1 };

    
    for(int i = 0; i < height - 1; i++)
    {
        for (int j = 1;j < width - 1;j++)
        {
     	    grey = 0;
     	    indexOffset = (i * width) + j;
            
            
            for(int k = 0; k < 4; ++k) {
                grey += src[indexOffset+indices[k]] >> 2;
            }
            
            dst[indexOffset] = (uint8_t) grey;
        }
    }
}

/**
 *  @author Allan Camargo (2013/1)
 *
 *  Execute threshold process on an image using NEON.
 *	Code adapted from the OpenCV library, method "cv::adaptiveThreshold"
 *
 *	@param _src 		Source data
 *	@param _dst 		Destination pointer
 *	@param maxValue		Maximum value to be assigned to a pixel
 *	@param blockSize
 *	@param delta
 */
void neonAdaptiveThreshold(cv::InputArray _src, cv::OutputArray _dst, unsigned char maxValue, int blockSize, int delta )
{
    cv::Mat src = _src.getMat();
    CV_Assert( src.type() == CV_8UC1 );
    CV_Assert( blockSize % 2 == 1 && blockSize > 1 );
    cv::Size size = src.size();
    
    _dst.create( size, src.type() );
    cv::Mat dst = _dst.getMat();
    
    if( maxValue < 0 )
    {
        dst = cv::Scalar(0);
        return;
    }
    
    cv::Mat mean;
    
    if( src.data != dst.data )
        mean = dst;
    
//	cv::boxFilter(src, mean, src.type(), cv::Size(blockSize, blockSize), cv::Point(-1,-1), true, cv::BORDER_REPLICATE);
//	asmNeonMeanFilter(src.data, mean.data, size.width, size.height);
    neonMeanFilter(src.data, mean.data, size.width, size.height);
    
    uchar imaxval = cv::saturate_cast<uchar>(maxValue);
    
    uint8_t* sdata = src.data;
    uint8_t* mdata = mean.data;
	uint8_t* ddata = dst.data;
    const uint8_t* sEnd = sdata + (size.width * size.height);
    
    const int8x8_t deltaN = vdup_n_s8(-delta);
    const uint8x8_t imaxvalN = vdup_n_u8(imaxval);
    const uint8x8_t zeroN = vdup_n_u8(0);
    
    while(sdata < sEnd)
    {
        const uint8x8_t sdataN = vld1_u8(sdata);
        const uint8x8_t mdataN = vld1_u8(mdata);
        const int8x8_t op1 = vsub_s8(sdataN, mdataN);
    	uint8x8_t mask = vcle_s8(op1, deltaN);
        vbsl_u8(mask, imaxvalN, zeroN);
        
        vst1_u8(ddata, mask);
        
        sdata += 8;
        mdata += 8;
        ddata += 8;
    }
}

void asmNeonAdaptiveThreshold(const cv::InputArray _src, cv::OutputArray _dst, unsigned char maxValue, const int delta)
{
    cv::Mat src = _src.getMat();
    CV_Assert( src.type() == CV_8UC1 );
    cv::Size size = src.size();
    
    _dst.create( size, src.type() );
    cv::Mat dst = _dst.getMat();
    
    if( maxValue < 0 )
    {
        dst = cv::Scalar(0);
        return;
    }
    
    cv::Mat mean;
    
    if( src.data != dst.data )
        mean = dst;
    
    asmNeonMeanFilter(src.data, mean.data, size.width, size.height);
    //boxFiltering(src.data, mean.data, size.width, size.height);
//    boxFiltering2(src.data, mean.data, size.width, size.height);
//    asmNeonBoxFilterBlurKernel(src.data, mean.data, size.width, size.height);
    
    uchar imaxval = cv::saturate_cast<uchar>(maxValue);
    
    __asm__ __volatile
  	("lsr       %[sz], %[sz], #3    \n" // dividing %[sz] by 8 since we'll process 8 pixels at a time
     "vdup.s8   d0, %[delta]        \n"
     "vdup.u8   d1, %[maxval]       \n"
     "vmov.u8   d2, #0              \n"
     "1:                            \n"
     "vld1.8    {d3}, [%[src]]!     \n"
     "vld1.8    {d4}, [%[m]]!       \n"
     "vsub.s8   d5, d3, d4          \n"
     "vcle.s8   d6, d5, d0          \n"
     "vbsl.u8   d6, d1, d2          \n"
     "vst1.8    {d6}, [%[dst]]!     \n"
     "subs      %[sz], %[sz], #1    \n" // decrement %[sz]
     "bne       1b                  \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     : [dst] "+r" (dst.data)
     : [src] "r" (src.data), [m] "r" (mean.data), [sz] "r" (size.width*size.height), [maxval] "r" (imaxval), [delta] "r" (-delta)
     : "memory", "cc");
}



void neonAdaptiveThreshold2Pass(const cv::InputArray _src, cv::OutputArray _dst, const unsigned char maxValue, const int delta)
{
    cv::Mat src = _src.getMat();
    CV_Assert( src.type() == CV_8UC1 );
    cv::Size size = src.size();
    
    _dst.create( size, src.type() );
    cv::Mat dst = _dst.getMat();
    
    if( maxValue < 0 )
    {
        dst = cv::Scalar(0);
        return;
    }
    
    cv::Mat pass1;
    cv::Mat pass2;
    
    if(src.data != dst.data)
    {
        pass1 = dst;
        pass2 = dst;
    }
    
    asmNeonBoxFilterBlurKernel(src.data, pass1.data, size.width, size.height);
    asmNeonBoxFilterBlurKernel(pass1.data, pass2.data, size.width, size.height);
    
    uchar imaxval = cv::saturate_cast<uchar>(maxValue);
    
    __asm__ __volatile
  	("lsr       %[sz], %[sz], #3    \n" // dividing %[sz] by 8 since we'll process 8 pixels at a time
     "vdup.s8   d0, %[delta]        \n"
     "vdup.u8   d1, %[maxval]       \n"
     "vmov.u8   d2, #0              \n"
     "1:                            \n"
     "vld1.8    {d3}, [%[src]]!     \n"
     "vld1.8    {d4}, [%[m]]!       \n"
     "vsub.s8   d5, d3, d4          \n"
     "vcle.s8   d6, d5, d0          \n"
     "vbsl.u8   d6, d1, d2          \n"
     "vst1.8    {d6}, [%[dst]]!     \n"
     "subs      %[sz], %[sz], #1    \n" // decrement %[sz]
     "bne       1b                  \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     : [dst] "+r" (dst.data)
     : [src] "r" (src.data), [m] "r" (pass2.data), [sz] "r" (size.width*size.height), [maxval] "r" (imaxval), [delta] "r" (-delta)
     : "memory", "cc");
}

/**
 *  @author Allan Camargo (2013/1)
 *
 *  Execute threshold process on an image using NEON.
 *	Code adapted from the OpenCV library, method "cv::adaptiveThreshold"
 *
 *	@param _src 		Source data
 *	@param _dst 		Destination pointer
 *	@param maxValue		Maximum value to be assigned to a pixel
 *	@param blockSize
 *	@param delta
 */
void neonAdaptiveThreshold(const cv::InputArray _src, cv::OutputArray _dst, unsigned char maxValue, const int delta)
{
    cv::Mat src = _src.getMat();
    CV_Assert( src.type() == CV_8UC1 );
    cv::Size size = src.size();
    
    _dst.create( size, src.type() );
    cv::Mat dst = _dst.getMat();
    
    if( maxValue < 0 )
    {
        dst = cv::Scalar(0);
        return;
    }
    
    cv::Mat mean;
    
    if( src.data != dst.data )
        mean = dst;
    
#ifdef iAR2_USE_ASM
    asmNeonBoxFilterBlurKernel(src.data, mean.data, size.width, size.height);
    //    asmNeonMeanFilter(src.data, mean.data, size.width, size.height);
#else
    //	cv::boxFilter(src, mean, src.type(), cv::Size(blockSize, blockSize), cv::Point(-1,-1), true, cv::BORDER_REPLICATE);
	neonMeanFilter(src.data, mean.data, size.width, size.height);
#endif
    
    uchar imaxval = cv::saturate_cast<uchar>(maxValue);
    
#ifdef iAR2_USE_ASM
    __asm__ __volatile
  	("lsr       %[sz], %[sz], #3    \n" // dividing %[sz] by 8 since we'll process 8 pixels at a time
     "vdup.s8   d0, %[delta]        \n"
     "vdup.u8   d1, %[maxval]       \n"
     "vmov.u8   d2, #0              \n"
     "1:                            \n"
     "vld1.8    {d3}, [%[src]]!     \n"
     "vld1.8    {d4}, [%[m]]!       \n"
     "vsub.s8   d5, d3, d4          \n"
     "vcle.s8   d6, d5, d0          \n"
     "vbsl.u8   d6, d1, d2          \n"
     "vst1.8    {d6}, [%[dst]]!     \n"
     "subs      %[sz], %[sz], #1    \n" // decrement %[sz]
     "bne       1b                  \n" // if condition flag "ne" (not equal to zero) then branch (jump) to 1
     : [dst] "+r" (dst.data)
     : [src] "r" (src.data), [m] "r" (mean.data), [sz] "r" (size.width*size.height), [maxval] "r" (imaxval), [delta] "r" (-delta)
     : "memory", "cc");
#else
    uint8_t* sdata = src.data;
    uint8_t* mdata = mean.data;
	uint8_t* ddata = dst.data;
    const uint8_t* sEnd = sdata + (size.width * size.height);
    
    const int8x8_t deltaN = vdup_n_s8(-delta);
    const uint8x8_t imaxvalN = vdup_n_u8(imaxval);
    const uint8x8_t zeroN = vdup_n_u8(0);
    
    while(sdata < sEnd)
    {
        const uint8x8_t sdataN = vld1_u8(sdata);
        const uint8x8_t mdataN = vld1_u8(mdata);
        const int8x8_t op1 = vsub_s8(sdataN, mdataN);
    	uint8x8_t mask = vcle_s8(op1, deltaN);
        vbsl_u8(mask, imaxvalN, zeroN);
        
        vst1_u8(ddata, mask);
        
        sdata += 8;
        mdata += 8;
        ddata += 8;
    }
#endif
}

/* OLD
void neonMeanFilter(uint8_t* src, uint8_t* dst, int width, int height)
{
    int indices[9] = { -(width + 1), -width, -(width - 1),
                                 -1,      0,    	    1,
                          width - 1,  width,    width + 1};
    
    uint8_t* p = src + width; //skip the first line
    uint8_t* d = dst + width;
    uint8_t* pEnd = src + (width * height);
    
    while(p < pEnd)
    {
        int i = 0;
        uint8x8_t greyAcc = vdup_n_u8(0);
        while (i < 9)
        {
            uint8_t* data = p + indices[i];
            
            uint8x8_t valuesN = vld1_u8(data);
            uint8x8_t shifted = vshr_n_u8(valuesN, 3);
            shifted = vsub_u8(valuesN, shifted);
            shifted = vshr_n_u8(shifted, 3);
            
            greyAcc = vadd_u8(greyAcc, shifted);
            
            ++i;
        }
        
        vst1_u8(d, greyAcc);
        
        p += 8;
        d += 8;
    }
}
 
void neon_asm_adaptive_threshold(cv::InputArray _src, cv::OutputArray _dst, unsigned char maxValue, int blockSize, double delta )
{
    cv::Mat src = _src.getMat();
    CV_Assert( src.type() == CV_8UC1 );
    CV_Assert( blockSize % 2 == 1 && blockSize > 1 );
    cv::Size size = src.size();
    
    _dst.create( size, src.type() );
    cv::Mat dst = _dst.getMat();
    
    if( maxValue < 0 )
    {
        dst = cv::Scalar(0);
        return;
    }
    
    cv::Mat mean;
    
    if( src.data != dst.data )
        mean = dst;
    
    //	cv::boxFilter(src, mean, src.type(), cv::Size(blockSize, blockSize), cv::Point(-1,-1), true, cv::BORDER_REPLICATE);
    neonMeanFilter(src.data, mean.data, size.width, size.height);
    
    int i, j;
    uchar imaxval = cv::saturate_cast<uchar>(maxValue);
    int idelta = cvFloor(delta);
    uchar tab[768];
    
    for( i = 0; i < 768; i++ )
    {
        tab[i] = (uchar)(i - 255 <= -idelta ? imaxval : 0);
    }
    
    if( src.isContinuous() && mean.isContinuous() && dst.isContinuous() )
    {
        size.width *= size.height;
        size.height = 1;
    }
    
    for( i = 0; i < size.height; i++ )
    {
        const uchar* sdata = src.data + src.step*i;
        const uchar* mdata = mean.data + mean.step*i;
        uchar* ddata = dst.data + dst.step*i;
        
        for( j = 0; j < size.width; j++ )
            ddata[j] = tab[sdata[j] - mdata[j] + 255];
    }
}
*/

// OLD
//void neonMeanFilter(uint8_t* src, uint8_t* dst, int width, int height)
//{
//    int indices[9] = { -(width + 1), -width, -(width - 1),
//						         -1,      0,    	    1,
//                          width - 1,  width,    width + 1};
//
//    uint8_t* p = src + width; //skip the first line
//    uint8_t* d = dst + width;
//    uint8_t* pEnd = src + (width * height);
//    float32x4_t weight = vdupq_n_f32(.111);
//
//    while(p < pEnd)
//    {
//        int i = 0;
//        static float values[4];
//        float32x4_t greyAcc = vdupq_n_f32(0); //just to be sure
//        while (i < 9)
//        {
//         	uint8_t* data = p + indices[i];
//
//            values[0] = *data++;
//            values[1] = *data++;
//            values[2] = *data++;
//            values[3] = *data++;
//
//            float32x4_t valuesN = vld1q_f32(values);
//
//    		greyAcc = vmlaq_f32(greyAcc, valuesN, weight); // multiply then accumulate into greyAcc
//
//            ++i;
//        }
//
//        vst1q_f32(values, greyAcc);
//
//        *d = values[0];
//        *(++d) = values[1];
//        *(++d) = values[2];
//        *(++d) = values[3];
//
//        p += 4;
//	    d++;
//    }
//}

#endif
