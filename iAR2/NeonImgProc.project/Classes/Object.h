/**
 * @file
 * @author  Jonathan Hess <jhess666@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Object model class.
 */

#import <GLTools.h>

#import "CppUtils.h"

class Object : public GLBatch {
public:
	float angle;
	M3DMatrix44f modelview;

	/**
	 * Construct instance of object.
	 */
	Object();

	/**
	 * Copy modelview matrix data.
	 *
	 * @param modelView The modelview matrix
	 */
	void copyModelview(M3DMatrix44d &modelview);

	/**
	 * Rotate 3D object at y axis.
	 *
	 * @param angle The angle value
	 */
	void rotate(float angle);
};
